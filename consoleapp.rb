require_relative './lib/fibonacci_multiplication_table'

puts "Enter fibonacci sequence length:"

fibonacci_sequence_length = gets.chomp.to_i
fibonacci_sequence_table  = MultiplicationTable.new(fibonacci_sequence_length)
fibonacci_sequence_table.multiplication_table.each {|row| p row.join(',').to_s }