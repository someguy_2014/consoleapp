class MultiplicationTable

  MULTI_TYPES = {
    fib:     ->(sequence_generator) { sequence_generator.create_fibonacci_sequence },
    default: ->(sequence_generator) { sequence_generator.create_default_sequence },
  }.freeze

  attr_reader :fsequence_length, :sequence_type, :sequence, :multiplication_table

  def initialize(sequence_length, sequence_type)
    @sequence_length      = sequence_length
    @sequence_type        = sequence_type
    @sequence             = create_sequence
    @multiplication_table = create_multiplication_table
  end

  private

  def create_sequence
    MULTI_TYPES[sequence_type].call(self)
  end

  def create_default_sequence
    (1..sequence_length).to_a
  end

  def create_fibonacci_sequence
    first_num  = 0
    second_num = 1
    [].tap do |fibonacci_sequence|
      fibonacci_sequence_length.times do
        fibonacci_sequence << first_num
        first_num, second_num = second_num, first_num + second_num
      end    
    end
  end

  def create_multiplication_table
    [].tap do |multiplication_table|
      multiplication_table << [nil, fibonacci_sequence].flatten
      fibonacci_sequence.each do |x|
        multiplication_table << [].tap do |row|
          row << x
          fibonacci_sequence.each do |y|
            row << x * y
          end
        end          
      end
    end
  end
end
