Gem::Specification.new do |s|
  s.name        = 'consoleapp'
  s.version     = '0.0.2'
  s.date        = '2020-11-07'
  s.summary     = 'consoleapp'
  s.description = 'fibonacci multiplication table'
  s.authors     = ['test test']
  s.email       = 'test@test.to'
  s.files       = Dir['lib/**/*.rb']
  s.homepage    = 'https://rubygems.org/gems/consoleapp'
  s.license       = 'MIT'
end
