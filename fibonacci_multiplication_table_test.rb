require "test/unit"
require "rack/test"
require_relative './lib/fibonacci_multiplication_table'

class FibonacciMultiplicationTableTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def test_fibonacci_sequence_length_is_set
    fibonacci_multiplication_table = FibonacciMultiplicationTable.new(4)

    assert_equal fibonacci_multiplication_table.fibonacci_sequence_length, 4
    assert_equal fibonacci_multiplication_table.fibonacci_sequence, [0, 1, 1, 2]
    assert_equal fibonacci_multiplication_table.multiplication_table, [[nil, 0, 1, 1, 2], [0, 0, 0, 0, 0], [1, 0, 1, 1, 2], [1, 0, 1, 1, 2], [2, 0, 2, 2, 4]]
  end
end
